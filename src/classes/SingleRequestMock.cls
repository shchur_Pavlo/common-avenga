/**
 * Created by pshchur on 12/22/2019.
 * @example
 * Test.setMock(HttpCalloutMock.class, new SingleRequestMock(200, 'OK', '{"param" : "value"}}', new Map<String, String>{'Content-Type' => 'application/json'}));
 */
@isTest
public class SingleRequestMock implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    public SingleRequestMock(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();

        for (String key :this.responseHeaders.keySet()) {
            res.setHeader(key, this.responseHeaders.get(key));
        }

        res.setBody(this.body);
        res.setStatusCode(this.code);
        res.setStatus(this.status);

        return res;
    }
}