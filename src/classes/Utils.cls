public with sharing class Utils {

    /**
    * Method returns boolean value whether field was changed
    * @param SObject rec: new record of the SObject which contains fields for checking
    * @param SObject old: old record of the SObject which contains fields for checking
    * @param List<String> fieldNames: list of fields for which values should be checked
    * @return Boolean: boolean value whether field was changed
    */
    public static Boolean fieldsChanged(SObject old, SObject rec, List<String> fieldNames) {
        Boolean isChanged = false;
        for (String fieldName :fieldNames) {
            if(rec.get( fieldName ) != old.get( fieldName )) {
                isChanged = true;
                break;
            }
        }
        return isChanged;
    }

    /**
    * Method returns boolean value whether field was changed
    * @param SObject rec: new record of the SObject which contains fields for checking
    * @param SObject old: old record of the SObject which contains fields for checking
    * @param String fieldName: field for which value should be checked
    * @return Boolean: boolean value whether field was changed
    */
    public static Boolean fieldsChanged(SObject old, SObject rec, String fieldName) {
        Boolean isChanged = false;
        if(rec.get( fieldName ) != old.get( fieldName )) {
            isChanged = true;
        }
        return isChanged;
    }
}