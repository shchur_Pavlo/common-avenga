/**
 * Created by pshchur on 12/29/2019.
 * @example
 * Map<String, HttpCalloutMock> reqMap = new Map <String, HttpCalloutMock>();
 * reqMap.put('https://firstEndPoint.com', new SingleRequestMock(200, 'OK', '{"param" : "value"}', new Map<String, String>{'Content-Type' => 'application/json'}));
 * reqMap.put('https://secondEndPoint.com', new SingleRequestMock(200, 'OK', '{"param" : "value"}', new Map<String, String>{'Content-Type' => 'application/json'}));
 * MultiRequestMock multiReqMock = new MultiRequestMock(reqMap);
 * Test.setMock(HttpCalloutMock.class, multiReqMock);
 */
@isTest
public class MultiRequestMock implements HttpCalloutMock {

    Map<String, HttpCalloutMock> requests;

    public MultiRequestMock(Map<String, HttpCalloutMock> requests) {
        this.requests = requests;
    }

    public HTTPResponse respond(HTTPRequest req) {
        HttpCalloutMock mock = requests.get(req.getEndpoint());
        if(mock == null) {
            for(String endpoint : requests.keySet()) {
                if(req.getEndpoint().contains(endpoint)) {
                    mock = requests.get(endpoint);
                }
            }
        }
        if(mock == null) {
            System.assert(false, 'MultiRequestMock Error: Unable to find Request with endpoint of '
                    + req.getEndpoint() + ' Make sure you have set up your mock data correctly');
        }
        return mock.respond(req);
    }

    public void addRequestMock(String url, HttpCalloutMock mock) {
        requests.put(url, mock);
    }
}