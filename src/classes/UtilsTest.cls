@IsTest
private class UtilsTest {

    //TODO Add message to System.assertEquals() methods. Comment is left by Pavlo.

    @IsTest
    static void fieldsChanged_multipleFieldsToProcess_oneFieldIsChanged() {
        Boolean expected = true;

        Account oldAccount = (Account) new SObjectBuilder(Account.SObjectType)
                .put(Account.Name, 'Old Account Name')
                .put(Account.Phone, '12345')
                .build().getRecord();
        Account newAccount = (Account) new SObjectBuilder(Account.SObjectType)
                .put(Account.Name, 'New Account Name')
                .put(Account.Phone, '12345')
                .build().getRecord();
        List<String> fieldNamesToCompare = new List<String> {'Name', 'Phone'};

        Boolean methodExecutionResult;

        Test.startTest();
            methodExecutionResult = Utils.fieldsChanged(oldAccount, newAccount, fieldNamesToCompare);
        Test.stopTest();

        System.assertEquals(expected, methodExecutionResult);
    }

    @IsTest
    static void fieldsChanged_multipleFieldsToProcess_multipleFieldsAreChanged() {
        Boolean expected = true;

        Account oldAccount = (Account) new SObjectBuilder(Account.SObjectType)
                .put(Account.Name, 'Old Account Name')
                .put(Account.Phone, '12345')
                .build().getRecord();
        Account newAccount = (Account) new SObjectBuilder(Account.SObjectType)
                .put(Account.Name, 'New Account Name')
                .put(Account.Phone, '54321')
                .build().getRecord();
        List<String> fieldNamesToCompare = new List<String> {'Name', 'Phone'};

        Boolean methodExecutionResult;

        Test.startTest();
            methodExecutionResult = Utils.fieldsChanged(oldAccount, newAccount, fieldNamesToCompare);
        Test.stopTest();

        System.assertEquals(expected, methodExecutionResult);
    }

    @IsTest
    static void fieldsChanged_multipleFieldsToProcess_noFieldsAreChanged() {
        Boolean expected = false;

        Account oldAccount = (Account) new SObjectBuilder(Account.SObjectType)
                .put(Account.Name, 'The Same Account Name')
                .put(Account.Phone, '12345')
                .build().getRecord();
        Account newAccount = (Account) new SObjectBuilder(Account.SObjectType)
                .put(Account.Name, 'The Same Account Name')
                .put(Account.Phone, '12345')
                .build().getRecord();
        List<String> fieldNamesToCompare = new List<String> {'Name', 'Phone'};

        Boolean methodExecutionResult;

        Test.startTest();
            methodExecutionResult = Utils.fieldsChanged(oldAccount, newAccount, fieldNamesToCompare);
        Test.stopTest();

        System.assertEquals(expected, methodExecutionResult);
    }

    @IsTest
    static void fieldsChanged_oneFieldToProcess_oneFieldIsChanged() {
        Boolean expected = true;

        Account oldAccount = (Account) new SObjectBuilder(Account.SObjectType).put(Account.Name, 'Old Account Name').build().getRecord();
        Account newAccount = (Account) new SObjectBuilder(Account.SObjectType).put(Account.Name, 'New Account Name').build().getRecord();
        String fieldNameToCompare = 'Name';

        Boolean methodExecutionResult;

        Test.startTest();
            methodExecutionResult = Utils.fieldsChanged(oldAccount, newAccount, fieldNameToCompare);
        Test.stopTest();

        System.assertEquals(expected, methodExecutionResult);
    }

    @IsTest
    static void fieldsChanged_oneFieldToProcess_fieldIsNotChanged() {
        Boolean expected = false;

        Account oldAccount = (Account) new SObjectBuilder(Account.SObjectType).put(Account.Name, 'The Same Account Name').build().getRecord();
        Account newAccount = (Account) new SObjectBuilder(Account.SObjectType).put(Account.Name, 'The Same Account Name').build().getRecord();
        String fieldNameToCompare = 'Name';

        Boolean methodExecutionResult;

        Test.startTest();
            methodExecutionResult = Utils.fieldsChanged(oldAccount, newAccount, fieldNameToCompare);
        Test.stopTest();

        System.assertEquals(expected, methodExecutionResult);
    }
}